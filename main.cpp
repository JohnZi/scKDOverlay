#include <windows.h>
#include <d3d9.h>
#include <iostream>
#include <d3dx9.h>
#include <Dwmapi.h> 
#include <TlHelp32.h>  
#include <string>
#include <tchar.h>
#include <fstream>
#include <strsafe.h>


//#define DEBUG

int s_width = 1920;
int s_height = 1080;
int Kills = 10;
int Deaths= 10;
int captures = 99;
int assists = 99;
TCHAR file[MAX_PATH];
char sKills[12] = "Kills:   \0"; 
char sDeaths[13] = "Deaths:    \0";
char sAssists[14] = "Assists:    \0";
char sCaptured[14] = "Beacons:    \0";
char kd[12]= "KD:     \0";
char * value;
char name[17] = { '\0' };
int nLine = 0;
std::string nick;
std::string killed = "Killed ";
#define CENTERX (GetSystemMetrics(SM_CXSCREEN) / 2) - (s_width / 2)
#define CENTERY (GetSystemMetrics(SM_CYSCREEN)/2)-(s_height/2)
LPDIRECT3D9 d3d;    // the pointer to our Direct3D interface
LPDIRECT3DDEVICE9 d3ddev;
HWND hWnd;
HWND newhwnd = NULL;
HWND g_HWND = NULL;
const MARGINS  margin = { 0,0,s_width,s_height };
LPD3DXFONT pFont;


LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void initD3D(HWND hWnd)
{
	d3d = Direct3DCreate9(D3D_SDK_VERSION);    // create the Direct3D interface

	D3DPRESENT_PARAMETERS d3dpp;    // create a struct to hold various device information

	ZeroMemory(&d3dpp, sizeof(d3dpp));    // clear out the struct for use
	d3dpp.Windowed = TRUE;    // program windowed, not fullscreen
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;    // discard old frames
	d3dpp.hDeviceWindow = hWnd;    // set the window to be used by Direct3D
	d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8;     // set the back buffer format to 32-bit
	d3dpp.BackBufferWidth = s_width;    // set the width of the buffer
	d3dpp.BackBufferHeight = s_height;    // set the height of the buffer

	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

	// create a device class using this information and the info from the d3dpp stuct
	d3d->CreateDevice(D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hWnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&d3dpp,
		&d3ddev);

	D3DXCreateFont(d3ddev, 17, 0, FW_BOLD, 1, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Arial", &pFont);

}

void DrawString(int x, int y, DWORD color, LPD3DXFONT g_pFont, const char *fmt)
{
	RECT FontPos = { x, y, x + 120, y + 16 };
	char buf[1024] = { '\0' };
	va_list va_alist;

	va_start(va_alist, fmt);
	vsprintf_s(buf, fmt, va_alist);
	va_end(va_alist);
	g_pFont->DrawText(NULL, buf, -1, &FontPos, DT_NOCLIP, color);
}


void render()
{
	// clear the window alpha
	d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0, 0, 0, 0), 1.0f, 0);

	d3ddev->BeginScene();    // begins the 3D scene
	sKills [8] = ('0' + Kills / 10);
	sKills [9] = ('0' + (Kills - (Kills / 10) * 10));
	sDeaths[9] = ('0' + Deaths / 10);
	sDeaths[10] = ('0' + (Deaths - (Deaths / 10) * 10));
	sAssists[10] = ('0' + assists / 10);
	sAssists[11] = ('0' + (assists - (assists / 10) * 10));
	sCaptured[10] = ('0' + captures / 10);
	sCaptured[11] = ('0' + (captures - (captures / 10) * 10));
	if(Deaths!=0){
		snprintf(kd + 4, 5, "%f", static_cast<float>(Kills) / static_cast<float>(Deaths));
	}
	else {
		kd[4] = '0' + Kills;
	}
	DrawString(17, s_height / 2 - 40, D3DCOLOR_ARGB(255, 0, 255, 0), pFont, sKills );
	DrawString(17, s_height / 2 - 20, D3DCOLOR_ARGB(255, 0, 255, 0), pFont, sAssists);
	DrawString(17, s_height / 2 , D3DCOLOR_ARGB(255, 0, 255, 0), pFont, sDeaths);
	DrawString(17, s_height / 2 + 20, D3DCOLOR_ARGB(255, 0, 255, 0), pFont, kd);
	DrawString(17, s_height / 2 + 40, D3DCOLOR_ARGB(255, 0, 255, 0), pFont, sCaptured);
	//std::cout << sDeaths << ", " << sKills << ", " << s_height << std::endl;
	
	d3ddev->EndScene();    // ends the 3D scene

	d3ddev->Present(NULL, NULL, NULL, NULL);   // displays the created frame on the screen
}

void clear() {
	d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0, 0, 0, 0), 1.0f, 0);
	d3ddev->BeginScene();
	d3ddev->EndScene();
	d3ddev->Present(NULL, NULL, NULL, NULL);
}


char* ReadINI(char* szSection, char* szKey, const char* szDefaultValue)
{
	LPCTSTR path = ".\\conf.ini";
	char* szResult = new char[255];
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(szSection, szKey,
		szDefaultValue, szResult, 255, ".\\conf.ini");
	return szResult;
}



BOOL CALLBACK EnumWindowsProcMy(HWND hwnd, LPARAM lParam)
{
	DWORD lpdwProcessId;
	GetWindowThreadProcessId(hwnd, &lpdwProcessId);
	if (lpdwProcessId == lParam)
	{
		g_HWND = hwnd;
		return FALSE;
	}
	return TRUE;
}

BOOL CALLBACK enumWindowsProc(
	__in  HWND hWnd,
	__in  LPARAM lParam
	) {

	int length = ::GetWindowTextLength(hWnd);
	if (0 == length) return TRUE;

	TCHAR* buffer;
	buffer = new TCHAR[length + 1];
	memset(buffer, 0, (length + 1) * sizeof(TCHAR));

	GetWindowText(hWnd, buffer, length + 1);
	std::string windowTitle = std::string(buffer);
	delete[] buffer;
	
	std::cout << hWnd << TEXT(": ") << windowTitle << std::endl;
	if (windowTitle.find("StarConflict") != std::string::npos) {
		for (int i = 0; i < 100; i++) { std::cout << "found!" << '\n'; }
		newhwnd = hWnd;
		value = new char[windowTitle.size() + 1];
		memcpy(value, windowTitle.c_str(), windowTitle.size() + 1);
	}
	std::cout.clear();
	return TRUE;
}

void getGame() {
	std::cout << TEXT("Enumerating Windows...") << std::endl;
	BOOL enumeratingWindowsSucceeded = ::EnumWindows(enumWindowsProc, NULL);
}

#pragma warning(disable : 4996)

void getUserName() {
	WIN32_FIND_DATA FindFileData;	
	HANDLE hFind;
	const char* User = { std::getenv("UserProfile") };
	std::cout << strlen(User) << std::endl;
	StringCchCopy(file, MAX_PATH, User);
	TCHAR pa[] = "\\Documents\\My Games\\StarConflict\\logs\\2*";
	int lenghtP = strlen(pa);
	int lenght = strlen(User);
	StringCchCat(file, MAX_PATH, "\\Documents\\My Games\\StarConflict\\logs\\2*" );

	std::cout << file << std::endl;
	hFind = FindFirstFile(file , &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE)
	{
		printf("FindFirstFile failed (%d)\n", GetLastError());
		return;
	}

	LARGE_INTEGER filesize;

	do
	{
#ifdef DEBUG
		if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			_tprintf(TEXT("  %s   <DIR>\n"), FindFileData.cFileName);
		}
		else
		{
			filesize.LowPart = FindFileData.nFileSizeLow;
			filesize.HighPart = FindFileData.nFileSizeHigh;
			_tprintf(TEXT("  %s   %ld bytes\n"), FindFileData.cFileName, filesize.QuadPart);


		}
#endif // DEBUG
	} while (FindNextFile(hFind, &FindFileData) != 0);


	{
		_tprintf(TEXT("The first file found is %s\n"),
			FindFileData.cFileName);
		std::cout << FindFileData.cFileName << std::endl;
		FindClose(hFind);
	}


	StringCchCopy(file, MAX_PATH, User);
	StringCchCat(file, MAX_PATH, "\\Documents\\My Games\\StarConflict\\logs\\");
	StringCchCat(file, MAX_PATH, FindFileData.cFileName);
	
	TCHAR GameLog[MAX_PATH] = {'\0'};
	StringCchCopy(GameLog, MAX_PATH, file);
	StringCchCat(GameLog, MAX_PATH, "\\game.log");


	std::ifstream LFile;
	std::string line;
	LFile.open(GameLog);
	if (LFile.is_open()) {
		while (std::getline(LFile, line)) {
			if ((line.find("MasterServerEndpoint: Successfully login to masterServer")) != std::string::npos) {
				auto pos = line.find("nick ");
				auto pos2 = line.find(", spaceStation");
				nick = line.substr(pos+5, pos2-pos-5);
				killed.append(nick);
				memcpy(name, nick.c_str(), nick.size() + 1);
				std::cout << nick <<std::endl;
				std::cout << pos2 << std::endl;
				std::cout << pos << std::endl;
				std::cin.get();
				goto close;
			}
		}
		close:
		LFile.close();
	}
	else {
		std::cerr << "File not open";
	}	
}


void getLogData(std::ifstream& in) 
{
		std::string line;
		if (in.is_open()) {
			
			while (std::getline(in, line)) {
				//std::cout << line << std::endl;
				if (line.find("Start gameplay") != std::string::npos) {
					Kills = 0;
					Deaths = 0;
					assists = 0;
					captures = 0;
					std::cout << "Start gameplay " << std::endl;
				}
				if (line.find("Gameplay finished") != std::string::npos) {
					//Kills = 0;
					//Deaths = 0;
					std::cout << "Gameplay finished" << std::endl;
				}
				size_t pos = line.find("JumpDockInvulnerabilityFreeSpace");
				if ((pos != std::string::npos) && (pos < line.find(name))) {
					//Kills = 0;
					//Deaths = 0;
					std::cout << "JumpDockInvulnerabilityFreeSpace ended" << std::endl;
				}

				pos = line.find("Killed ");
				if ((pos != std::string::npos) && (line.find(name,pos+40) != std::string::npos) && (line.find("Ship_Race",pos+6) != std::string::npos)) {
					Kills++;
					std::cout << "You killed someone" << std::endl;
				}

				if ((line.find(killed) != std::string::npos)) {
					Deaths++;
					std::cout << "You died" << std::endl;
				}

				pos = line.find("Captured 'VitalPoint_Beacon");
				if ((pos != std::string::npos) && (line.find(name,pos+1) != std::string::npos)) {
					captures++;
					std::cout << "You captured a beacon" << std::endl;
				}

				pos = line.find("Participant");
				if ((pos != std::string::npos) && (line.find(name, pos + 1) != std::string::npos)) {
					assists++;
					std::cout << "You assisted an kill" << std::endl;
				}


			}
		}
		else {
			std::cerr << "File not open";
	}
		in.clear();
		return;
}



int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	//char * value = ReadINI("CONFIGURATION", "WIN_NAME", "none");
#ifdef DEBUG
	if (TRUE) {
		AllocConsole();
		freopen("CONIN$", "r", stdin);
		freopen("CONOUT$", "w", stdout);
		freopen("CONOUT$", "w", stderr);
	}
#endif
	
	RECT rc;

	getGame();
	if (newhwnd != NULL) {
		GetWindowRect(newhwnd, &rc);
		s_width = rc.right - rc.left;
		s_height = rc.bottom - rc.top;
	}
	else {
		system("echo Star Conflict Window was not found && pause");
		std::cin.get();
		delete[] value;
		ExitProcess(0);
	}
	WNDCLASSEX wc;

	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = hInstance;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)RGB(0, 0, 0);
	wc.lpszClassName = "WindowClass";

	RegisterClassEx(&wc);

	hWnd = CreateWindowEx(0,
		"WindowClass",
		"",
		WS_EX_TOPMOST | WS_POPUP,
		rc.left, rc.top,
		s_width, s_height,
		NULL,
		NULL,
		hInstance,
		NULL);

	SetWindowLong(hWnd, GWL_EXSTYLE, (int)GetWindowLong(hWnd, GWL_EXSTYLE) | WS_EX_LAYERED | WS_EX_TRANSPARENT);
	SetLayeredWindowAttributes(hWnd, RGB(0, 0, 0), 0, ULW_COLORKEY);
	SetLayeredWindowAttributes(hWnd, 0, 255, LWA_ALPHA);

	ShowWindow(hWnd, nCmdShow);
	RegisterHotKey(
		NULL,
		1,
		MOD_CONTROL,
		0x77);
	bool vis = TRUE;
	getUserName();

	std::ifstream LFile;
	TCHAR CombatLog[MAX_PATH] = { '\0' };
	StringCchCopy(CombatLog, MAX_PATH, file);
	StringCchCat(CombatLog, MAX_PATH, "\\combat.log");
	LFile.open(CombatLog);

	initD3D(hWnd);
	MSG msg;
	::SetWindowPos(FindWindow(NULL, value), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	while (TRUE)
	{
		::SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

		if (!FindWindow(NULL, value)) {
			GetLastError();
			delete[] value;
			ExitProcess(0);
		}
			
		if (vis)
			render();
		else
			clear();

		getLogData(LFile);

		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_HOTKEY)
			{
				if (vis) {
					vis = FALSE;
					clear();
				}
				else
					vis = TRUE;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT) {
			delete[] value;
			UnregisterHotKey(NULL, 1);
			exit(0);
		}
			

	}


	delete[] value;
	return msg.wParam;
}
#pragma warning(enable : 4996)


LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_PAINT:
	{
		DwmExtendFrameIntoClientArea(hWnd, &margin);
	}break;

	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	} break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}
